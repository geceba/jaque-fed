import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { User } from './model/user.model';
import { Role } from './model/role.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseURL: string = environment.base;
  
  constructor(public http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${this.baseURL}users.json`);
  }

  getRoles(): Observable<Role[]> {
    return this.http.get<Role[]>(`${this.baseURL}roles.json`);
  }
}
