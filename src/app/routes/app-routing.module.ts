import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from '../core/pages/users/users.component';
import { UsersModule } from '../core/pages/users/users.module';

const routes: Routes = [
  { 
    path: 'users',
    component: UsersComponent,
    loadChildren: () => import('../core/pages/users/users.module').then(m => m.UsersModule ) 
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
