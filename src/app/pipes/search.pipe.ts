import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(users: any, search: string = ''): unknown {
    return users.filters(user => {
      return !!search ? user.name.toLowerCase() === search : user;
    });
  }

}
