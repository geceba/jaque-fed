import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './users.component';

import { CustomButtonComponent } from '../../components/atoms/custom-button/custom-button.component';
import { CardComponent } from '../../components/card/card.component';
import { ErrorMessagePanelComponent } from '../../components/atoms/error-message-panel/error-message-panel.component';

import { ListUsersComponent } from '../../components/list-users/list-users.component';
import { FormComponent } from '../../components/form/form.component';
import { SearchFilterComponent } from '../../components/search-filter/search-filter.component';

@NgModule({
  declarations: [
    UsersComponent,
    CustomButtonComponent,
    CardComponent,
    ErrorMessagePanelComponent,
    ListUsersComponent,
    FormComponent,
    SearchFilterComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class UsersModule { }
