import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  show:boolean = false

  constructor() { }

  ngOnInit(): void {
  }

  open(e: any) {
    this.show = e;
  }

  close(e: any) {
    this.show = !e;
  }

}
