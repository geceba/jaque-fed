import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/service/model/user.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  title: string = '';
  @Output() close = new EventEmitter<boolean>();
  @Input() user:any = {};

  userForm = new FormGroup({
    name: new FormControl('', Validators.required),
    lastnameP: new FormControl('', Validators.required),
    lastnameM: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email])
  })

  constructor() { }

  ngOnInit(): void {
    this.loadUser();
  }

  loadUser() {
    console.log(this.user);
    
  }

  closeDialog() {
    this.close.emit(true);
  }

}
