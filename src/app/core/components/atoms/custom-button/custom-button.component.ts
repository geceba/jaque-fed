import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-custom-button',
  templateUrl: './custom-button.component.html',
  styleUrls: ['./custom-button.component.scss']
})
export class CustomButtonComponent implements OnInit {

  @Input()
  className: string = 'add';
  @Input()
  title: string = '';

  @Output() openDialog = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  open() {
    if(!!this.openDialog) {
      this.openDialog.emit(true);
    }
  }

}
