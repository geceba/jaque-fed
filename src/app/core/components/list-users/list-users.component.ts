import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/service/model/user.model';
import { UserService } from '../../../service/user.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {

  users: User[];
  show:boolean = false
  userSelected: any;

  constructor(private _userService: UserService) { 
    this.users = [];
    this.userSelected ={};
  }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers() {
    return this._userService.getUsers().subscribe(
      (response: any) => { 
        this.users = response.users;   
      },
      (error) => {
        console.log('error: ', error);
      }
    )
  }

  removeUser(index: number) {
    if(!!this.users) {
      this.users.splice(index, 1);
    }
  }

  editUser(e:any, user: User) {
    this.userSelected = user;
    this.open(e);
  }

  open(e: any) {
    this.show = e;
  }

  close(e: any) {
    this.show = !e;
  }

}
